# Original PS1 value:
# \[\e]0;\u@\h: \w\a\]${debian_chroot:+($debian_chroot)}\u@\h:\w\$

# Give colors to the prompt
export PS1="\[\e[33;40m\]\D{%H:%M:%S} \[\e[36m\]\u\[\e[37m\]@\[\e[32m\]\h\[\e[37m\]:\[\e[35m\]\w\[\e[0m\] \$ "

# Frequently used aliases
alias cdans='cd /ansible'
alias cdcfg='cd /ansible/config'
alias cdfile='cd /ansible/files'
alias cdinv='cd /ansible/inventory'
alias cdlog='cd /ansible/logs'
alias cdplay='cd /ansible/playbooks'
alias cdrole='cd /ansible/roles'
alias cdscr='cd /ansible/scripts'
alias cdsec='cd /ansible/secrets'
alias cdsrc='cd /ansible/source'
alias cdssh='cd /ansible/ssh'
alias cdtmp='cd /ansible/temp'
alias cdvault='cd /ansible/vault_keys'

alias ll='ls -l'
alias llh='ls -lh'
alias lla='ls -la'
alias lld='ls -ld'
alias psg='ps -ef|grep -i'
alias up='cd ..'
