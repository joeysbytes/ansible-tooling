#!/bin/bash
set -e

source common_fingerprint.bash


function main() {
    echo "Delete fingerprint from known hosts file"
    initialize "$@"
    delete_fingerprint_from_known_hosts
}


function delete_fingerprint_from_known_hosts() {
    echo "Deleting fingerprint to known hosts file"
    ssh-keygen -f "${KNOWN_HOSTS_FILE}" -R "${HOST_ADDRESS}"
}


main "$@"
