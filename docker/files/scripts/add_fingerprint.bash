#!/bin/bash
set -e

source common_fingerprint.bash
SSH_KEYSCAN_TIMEOUT=20
TEMP_KNOWN_HOSTS_FILE="/tmp/temp_known_hosts"


function main() {
    echo "Add fingerprint to known hosts file"
    initialize "$@"
    add_fingerprint_to_known_hosts
}


function add_fingerprint_to_known_hosts() {
    # https://fixyacloud.wordpress.com/2020/01/26/can-i-automatically-add-a-new-host-to-known_hosts/
    # Solution 4
    echo "Adding fingerprint to known hosts file"
    ssh-keyscan -T "${SSH_KEYSCAN_TIMEOUT}" "${HOST_ADDRESS}"|sort --unique - "${KNOWN_HOSTS_FILE}">"${TEMP_KNOWN_HOSTS_FILE}"
    mv "${TEMP_KNOWN_HOSTS_FILE}" "${KNOWN_HOSTS_FILE}"
    chmod 600 "${KNOWN_HOSTS_FILE}"
}


main "$@"
