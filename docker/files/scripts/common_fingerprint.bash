KNOWN_HOSTS_FILE=""
HOST_ADDRESS=""


function initialize() {
    local number_of_args=$#
    local script_name="$0"

    # Verify number of arguments
    if [ $number_of_args -lt 1 ] || [ $number_of_args -gt 2 ]; then
        echo "USAGE: ${script_name} hostAddress [knownHostsFile]">&2
        exit 1
    fi

    # Set host address
    HOST_ADDRESS="${1}"
    echo "Host Address:     ${HOST_ADDRESS}"

    # Set known hosts file
    if [ $# -eq 2 ]; then
        KNOWN_HOSTS_FILE="${2}"
    else
        # If no known hosts file is provided, use user default
        KNOWN_HOSTS_FILE="${HOME}/.ssh/known_hosts"
    fi
    echo "Known Hosts File: ${KNOWN_HOSTS_FILE}"
    if [ ! -f "${KNOWN_HOSTS_FILE}" ]; then
        echo "Known hosts file not found, creating"
        touch "${KNOWN_HOSTS_FILE}"
        chmod 600 "${KNOWN_HOSTS_FILE}"
    fi
}
