import json
import sys
import urllib.request
from typing import List


def main() -> None:
    print("Checking for Ansible version")
    env_file = sys.argv[1]
    env_data = get_env_file_data(env_file)
    current_ans_ver = get_env_data_value("ANS_VER", env_data)
    check_ansible_version = get_env_data_value("UPDATE_ANS_VER_FROM_INTERNET", env_data)
    if check_ansible_version.lower() == "true":
        ans_internet_ver = get_latest_ansible_version_from_internet()
        print(f"  Found version: {ans_internet_ver}")
        if ans_internet_ver != current_ans_ver:
            set_env_data_value("ANS_VER", ans_internet_ver, env_data)
            set_env_file_data(env_file, env_data)
        else:
            print(f"  Environment file already at this version")
    else:
        print("  Ansible version will be used from environment file")
        print(f"  Ansible version: {current_ans_ver}")


def get_env_file_data(env_file: str) -> List[str]:
    """Read the contents of the env file and return them."""
    print(f"  Reading in environment file: {env_file}")
    with open(env_file, 'r') as e_file:
        env_data = e_file.readlines()
    return env_data


def set_env_file_data(env_file: str, env_data: List[str]):
    """Given a env file name and data, write out the updated data."""
    print(f"  Writing out environment file: {env_file}")
    with open(env_file, 'w') as e_file:
        e_file.writelines(env_data)


def get_env_data_value(env_key: str, env_data: List[str]) -> str:
    """Given a key and the env data, return the value."""
    print(f"  Getting environment value for key: {env_key}")
    env_value = None
    for env_line in env_data:
        if env_line.startswith(env_key + "="):
            env_value = env_line.split('=')[1]
            break
    # remove surrounding quotes, if any
    if env_value.startswith('"'):
        env_value = env_value.split('"')[1]
    if env_value.startswith("'"):
        env_value = env_value.split("'")[1]
    return env_value


def set_env_data_value(env_key: str, env_value: str, env_data: List[str]) -> None:
    """Given a key and value, set this in the environment data"""
    print(f"  Setting environment key: {env_key}, to value: {env_value}")
    data = f'{env_key}="{env_value}"\n'
    for i in range(0, len(env_data)):
        if env_data[i].startswith(env_key + "="):
            env_data[i] = data
            break


def get_latest_ansible_version_from_internet() -> str:
    """Go to the internet, determine the latest Ansible version available to install."""
    print("  Ansible version will be retrieved from Internet")
    url = "https://pypi.org/pypi/ansible/json"
    request = urllib.request.urlopen(url)
    json_data = request.read().decode('utf-8')
    dict_data = json.loads(json_data)
    ansible_version = dict_data["info"]["version"]
    return ansible_version


main()
