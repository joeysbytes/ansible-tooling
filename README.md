# Joey's Bytes Ansible Tooling

## Purpose / Description

This intent of this project is to create a portable, flexible, and consistent Ansible environment. It can be run
interactively within its own bash environment, or used within a CI/CD process (scripting). 

This is not packaged to be a "product" per se, but more as a template you can customize for your own needs. As such,
it is [licensed](./LICENSE) under the [Unlicense](https://unlicense.org).
