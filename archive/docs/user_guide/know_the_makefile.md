# User Guide - Know the Makefile: Joey's Bytes Ansible Tooling

[[_TOC_]]

## Makefile Variables

The number of variables required by the Makefile has been minimized for ease of use. Internally, there are more
variables being used, but only these variables should need to be modified as needed:

| Variable Name       | Default Value                            | Description                                                                                                          |
|---------------------|------------------------------------------|----------------------------------------------------------------------------------------------------------------------|
| BUILD_CONFIG_FILE   | _empty string_                           | (Optional) The JSON file name for the build configuration values.                                                    |
| CONFIG_VALUES_FILE  | /tmp/ansible_tooling_build_config.json   | An automatically-generated JSON file containing the final build configuration values used to build the Docker image. |
| RUN_CONFIG_FILE     | _empty string_                           | (Optional) The JSON filename for the running (docker compose) values.                                                |
| DOCKER_COMPOSE_FILE | /tmp/ansible_tooling_docker-compose.json | An automatically-generated docker compose JSON file.                                                                 |
| PYTHON3_EXECUTABLE  | python3                                  | The binary for Python 3 that you wish to use. Some distributions may use 'python'.                                   |

## Target: help (or no target)

Forgot a Makefile target? No problem, type 'make' by itself, and a quick-help screen will be displayed.

```bash
make
make help
```

## Target: build

This target builds the Docker image, utilizing the build config values specified.
See the [documentation](./build_ansible_tooling_image.md) on building the Ansible Tooling Docker image.

```bash
make build

# These are the individual build steps, in order
make build_config_values
make build_docker_image
make build_docker_compose
```

<hr />

[User Guide](../user_guide.md) - [Main README](../../README.md)
