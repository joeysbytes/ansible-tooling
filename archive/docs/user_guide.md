# User Guide: Joey's Bytes Ansible Tooling

## Requirements

These are the requirements needed on the computer running Ansible Tooling:

* bash
* make
* Docker (with Docker Compose installed)
* A semi-recent version of Python 3

## How Ansible Tooling Works

Ansible Tooling is a Docker image, with a full bash and installation environment set up. It is configuration-driven,
to the point that you build and run the image from JSON files, rather than DockerFile and docker_compose (this is
handled for you). And to further simplify the process, all steps can be performed using make.

To use Ansible Tooling, perform these steps:

1. [Step 1](./user_guide/know_the_makefile.md): Understand the Makefile
2. [Step 2](./user_guide/build_ansible_tooling_image.md): Configure and run the building of the Docker image.

[Back to Ansible Tooling README](../README.md)
