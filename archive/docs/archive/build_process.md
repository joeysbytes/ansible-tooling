# The Build Process

Using "make build" performs the following steps.

## Step 1: Building Configuration Values

The file "./build/config/build_config.json" contains values used to build the concrete configuration values
needed to build the Docker image. The values are documented as follows:

| Key                      | Description                                                                               |
|--------------------------|-------------------------------------------------------------------------------------------|
| docker / python_version  | From Dockerhub, this is the Python version to start with                                  |
| docker / debian_codename | From Dockerhub, this is the Debian version tied with the Python version                   |
| host / user_name         | If the user id needs to be searched for, this is the user to use                          |
| host / group_name        | If the group id needs to be searched for, this is the group to use                        |
| host / user_id           | If this is -1, it will be searched for by user_name, otherwise this value is used         |
| host / group_id          | If this is -1, it will be searched for by group_name, otherwise this value is used        |
| ansible / version        | If this is "latest", the internet will be searched for the latest Ansible release version |

When processed, the file "./build/config/config_values" will be output. This file is used by the rest of the build
processes.
