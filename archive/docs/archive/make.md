# Makefile Documentation

The Makefile is designed to give you all necessary functionality. Typing "make" by itself will give you a help
screen with the available targets.

It is assumed you are in the directory of the Makefile when running any make commands.

## Configuration Variables

### Build / Configuration Files

| Variable Name      | Default Value      | Description                                     |
|--------------------|--------------------|-------------------------------------------------|
| BUILD_CONFIG_FILE  | build_config.json  | Build configuration file                        |
| CONFIG_VALUES_FILE | config_values.json | Name of auto-generated configuration file       |
| PYTHON3_EXECUTABLE | python3            | The file name of the Python 3 executable to use |
| RUN_ENV_FILE       | ansible.env        | Name of environment file for docker compose     |
