#!/bin/bash

# Show information about this container

echo "OS VERSION:"
grep "PRETTY_NAME=" "/etc/os-release"

echo ""
echo "PYTHON VERSION:"
python --version

echo ""
echo "ANSIBLE VERSION:"
ansible --version
