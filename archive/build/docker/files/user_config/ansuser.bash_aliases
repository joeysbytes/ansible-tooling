# Original PS1 value:
# \[\e]0;\u@\h: \w\a\]${debian_chroot:+($debian_chroot)}\u@\h:\w\$

# Give colors to the prompt
export PS1="\[\e[36;40m\]\u\[\e[33;40m\]@\[\e[32;40m\]\h\[\e[33;40m\]:\[\e[35;40m\]\w\[\e[0m\] \$ "

# Frequently used aliases
alias ll='ls -l'
alias llh='ls -lh'
alias lla='ls -la'
alias lld='ls -ld'
alias psg='ps -ef|grep -i'
alias up='cd ..'
