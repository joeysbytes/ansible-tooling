import os
from pathlib import Path
import sys
from typing import Dict, List

from utils import screen_output_utils as screen
from utils import bash_utils as bash
from data_files.JsonFile import JsonFile
from config.build_defaults import BUILD_DEFAULTS

###################################################################################################################


def main():
    screen.heading_1("Build Docker Image")

    # get config values
    config_values_file = sys.argv[1]
    if config_values_file == "":
        config_values_file = BUILD_DEFAULTS["files"]["config_values"]
    config_values = JsonFile(file_name=config_values_file)
    config_values.load()

    # change to directory of Dockerfile
    docker_file = sys.argv[2]
    docker_file_dir = str(Path(docker_file).parent)
    print(f"Docker file dir: {docker_file_dir}")
    os.chdir(docker_file_dir)

    # build docker image
    cmd = get_docker_build_command(config_values.data)
    print("Running Docker build command\n")
    bash.run_in_shell(cmd)


###################################################################################################################


def get_docker_build_command(config: Dict) -> List:
    """Given the config data, build the command to build the Docker image."""
    print("Building command to build docker image")
    cmd = ["docker", "build", "--pull", "--progress", "plain"]

    # tags
    is_latest_version = config['ansible']['latest_version']
    cmd.extend(["--tag", f"{config['docker']['org_name']}/ansible:{config['ansible']['version']}"])
    if is_latest_version:
        cmd.extend(["--tag", f"{config['docker']['org_name']}/ansible:latest"])

    # build args
    cmd.extend(["--build-arg", f"PYTHON_VERSION={config['docker']['python_version']}"])
    cmd.extend(["--build-arg", f"DEBIAN_CODENAME={config['docker']['debian_codename']}"])
    cmd.extend(["--build-arg", f"TIME_ZONE={config['docker']['time_zone']}"])
    cmd.extend(["--build-arg", f"USER_ID={config['controller']['user_id']}"])
    cmd.extend(["--build-arg", f"GROUP_ID={config['controller']['group_id']}"])
    cmd.extend(["--build-arg", f"ANSIBLE_VERSION={config['ansible']['version']}"])

    # Dockerfile
    cmd.append(".")

    return cmd

###################################################################################################################


if __name__ == "__main__":
    main()
