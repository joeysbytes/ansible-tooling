from abc import ABC, abstractmethod
from pathlib import Path
import sys
from typing import Any

from utils import screen_output_utils as screen


class BaseFile(ABC):

    def __init__(self, file_name: str, read_only: bool = True):
        self._file_path = Path(file_name)
        self._data = None
        self._read_only = read_only

    def __str__(self):
        text = f"File Path: {self.file_path}\n"
        text += f"Read Only: {self.read_only}\n"
        text += "Data:\n"
        text += f"{self.data}"
        return text

    @property
    def read_only(self) -> bool:
        return self._read_only

    @property
    def file_path(self) -> Path:
        return self._file_path

    @property
    def file_name(self) -> str:
        return str(self.file_path)

    @property
    def data(self) -> Any | None:
        return self._data

    @data.setter
    def data(self, the_data: Any) -> None:
        self._data = the_data

    def load(self) -> None:
        print(f"Loading data from file: {self.file_name}")
        with open(str(self.file_path), 'r') as f:
            data = f.read()
        self.set_loaded_data(data)

    @abstractmethod
    def set_loaded_data(self, data: str) -> None:
        pass

    def save(self) -> None:
        print(f"Saving data to file: {self.file_name}")
        if self.read_only:
            screen.error(f"Data file is read only: {self.file_name}")
            sys.exit(1)
        data = self.format_save_data()
        with open(self.file_name, 'w') as f:
            f.write(data)

    @abstractmethod
    def format_save_data(self) -> str:
        pass
