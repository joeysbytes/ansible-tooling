from typing import Any

from data_files.BaseFile import BaseFile


class EnvFile(BaseFile):

    def set_loaded_data(self, data: str) -> None:
        raise NotImplemented("Need to code loading EnvFile, if it's ever used.")

    def format_save_data(self) -> str:
        data = ""
        for entry in self.data:
            key = entry["key"]
            value = entry["value"]
            data += f'{key}='
            if isinstance(value, str):
                data += '"'
            data += f'{value}'
            if isinstance(value, str):
                data += '"'
            data += '\n'
        return data

    def add_entry(self, key: str, value: Any) -> None:
        if self.data is None:
            self.data = []
        entry = {"key": key, "value": value}
        self.data.append(entry)
