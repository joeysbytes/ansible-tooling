import json

from data_files.BaseFile import BaseFile


class JsonFile(BaseFile):

    def set_loaded_data(self, data: str) -> None:
        self.data = json.loads(data)

    def format_save_data(self) -> str:
        return json.dumps(self.data, sort_keys=True, indent=2)
