import sys


def heading_1(text: str) -> None:
    """Given a text line, print it with a text underline."""
    underline = '='
    heading = text + '\n'
    heading += underline * len(text)
    print(heading)


def error(text: str) -> None:
    """Given a text line, wrap it with an error label and print it to stderr."""
    err = f"ERROR: {text}"
    print(err, file=sys.stderr)
