import subprocess
from typing import Dict, List


def run_command(cmd: List) -> Dict:
    """Given a bash command list, run it and return the results."""
    output = {}
    result = subprocess.run(cmd, shell=False, capture_output=True, check=True)
    output["stdout"] = str(result.stdout, encoding='utf-8')
    output["stderr"] = str(result.stderr, encoding='utf-8')
    output["rc"] = result.returncode
    return output


def run_in_shell(cmd: List) -> None:
    """Given a bash command list, run it in its own process."""
    bash_cmd = ["bash", "-c"]
    bash_cmd.extend(cmd)
    subprocess.run(cmd, shell=False, capture_output=False, check=True)
