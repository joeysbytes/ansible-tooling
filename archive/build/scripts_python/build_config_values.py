import json
import sys
from typing import Dict, Tuple
import urllib.request

from utils import screen_output_utils as screen
from utils import bash_utils as bash
from data_files.JsonFile import JsonFile
from config.build_defaults import BUILD_DEFAULTS


###################################################################################################################


def main():
    screen.heading_1("Build Config Values")

    # get build config data
    build_config_file = sys.argv[1]
    build_config_data = {}
    if build_config_file == "":
        print("No build configuration file provided")
    else:
        build_config = JsonFile(file_name=build_config_file)
        build_config.load()
        build_config_data = build_config.data

    # create config values
    config_values_data = build_config_values_data(build_config_data)

    # save config values
    config_values_file = sys.argv[2]
    if config_values_file == "":
        config_values_file = BUILD_DEFAULTS["files"]["config_values"]
    config_values = JsonFile(file_name=config_values_file, read_only=False)
    config_values.data = config_values_data
    config_values.save()


###################################################################################################################


def build_config_values_data(build_config_data: Dict) -> Dict:
    """Given the build config data, create the config values data."""
    print("Building config values data")

    # We populate each value individually so key errors will be thrown if something is missing.
    data = {"docker": {},
            "controller": {},
            "ansible": {}}

    # Docker values
    data["docker"]["python_version"] = get_docker_python_version(build_config_data)
    data["docker"]["debian_codename"] = get_docker_debian_codename(build_config_data)
    data["docker"]["org_name"] = get_docker_org_name(build_config_data)
    data["docker"]["time_zone"] = get_docker_time_zone(build_config_data)

    # Controller values
    data["controller"]["user_id"] = get_controller_user_id(build_config_data)
    data["controller"]["group_id"] = get_controller_group_id(build_config_data)

    # Ansible values
    data["ansible"]["version"], data["ansible"]["latest_version"] = get_ansible_version(build_config_data)

    return data


# -----------------------------------------------------------------------------------------------------------------
# Populate build config values
# -----------------------------------------------------------------------------------------------------------------

def get_docker_python_version(build_config_data: Dict) -> str:
    """Return the Python version to use for the Docker image."""
    python_version = BUILD_DEFAULTS["docker"]["python_version"]
    if ("docker" in build_config_data) and ("python_version" in build_config_data["docker"]):
        python_version = build_config_data["docker"]["python_version"]
    print(f"Docker Python Version: {python_version}")
    return python_version


def get_docker_debian_codename(build_config_data: Dict) -> str:
    """Return the Debian version (by codename) to use for the Docker image."""
    debian_codename = BUILD_DEFAULTS["docker"]["debian_codename"]
    if ("docker" in build_config_data) and ("debian_codename" in build_config_data["docker"]):
        debian_codename = build_config_data["docker"]["debian_codename"]
    print(f"Docker Debian Codename: {debian_codename}")
    return debian_codename


def get_docker_org_name(build_config_data: Dict) -> str:
    """Return the organization name desired in the Docker image tag name."""
    org_name = BUILD_DEFAULTS["docker"]["org_name"]
    if ("docker" in build_config_data) and ("org_name" in build_config_data["docker"]):
        org_name = build_config_data["docker"]["org_name"]
    print(f"Docker image tag org name: {org_name}")
    return org_name


def get_docker_time_zone(build_config_data: Dict) -> str:
    """Return the time zone desired for use internal to the Docker image."""
    time_zone = BUILD_DEFAULTS["docker"]["time_zone"]
    if ("docker" in build_config_data) and ("time_zone" in build_config_data["docker"]):
        time_zone = build_config_data["docker"]["time_zone"]
    print(f"Docker image time zone: {time_zone}")
    return time_zone


def get_controller_user_id(build_config_data: Dict) -> int:
    """Return the user id for ansuser within the Docker image."""
    is_id_by_default = True
    user_id = BUILD_DEFAULTS["controller"]["user_id"]
    user_name = ""
    if ("controller" in build_config_data) and ("user_id" in build_config_data["controller"]):
        user_id = build_config_data["controller"]["user_id"]
        is_id_by_default = False
    if ("controller" in build_config_data) and ("user_name" in build_config_data["controller"]):
        user_name = build_config_data["controller"]["user_name"]
    if (user_name != "") and is_id_by_default:
        user_id = get_user_id_by_user_name(user_name)
    print(f"ansuser user id: {user_id}")
    return user_id


def get_controller_group_id(build_config_data: Dict) -> int:
    """Return the group id for ansgrp within the Docker image."""
    is_id_by_default = True
    group_id = BUILD_DEFAULTS["controller"]["group_id"]
    group_name = ""
    if ("controller" in build_config_data) and ("group_id" in build_config_data["controller"]):
        group_id = build_config_data["controller"]["group_id"]
        is_id_by_default = False
    if ("controller" in build_config_data) and ("group_name" in build_config_data["controller"]):
        group_name = build_config_data["controller"]["group_name"]
    if (group_name != "") and is_id_by_default:
        group_id = get_group_id_by_group_name(group_name)
    print(f"ansgrp group id: {group_id}")
    return group_id


def get_ansible_version(build_config_data: Dict) -> Tuple[str, bool]:
    """Return the Ansible version requested, and whether or not the latest version has been verified."""
    ansible_version = BUILD_DEFAULTS["ansible"]["version"]
    ansible_latest_version = False
    if ("ansible" in build_config_data) and ("version" in build_config_data["ansible"]):
        ansible_version = build_config_data["ansible"]["version"]
    # If ansible version = "latest", go find the latest version from the internet
    if ansible_version == "latest":
        ansible_version = get_latest_ansible_version_from_internet()
        ansible_latest_version = True
    print(f"Ansible version: {ansible_version}")
    print(f"Ansible verified latest version: {ansible_latest_version}")
    return ansible_version, ansible_latest_version


# -----------------------------------------------------------------------------------------------------------------
# Get external data
# -----------------------------------------------------------------------------------------------------------------

def get_user_id_by_user_name(user_name: str) -> int:
    """Given a user name, return its user id"""
    print(f"Getting user id by user name: {user_name}")
    cmd = ["id", "-u", user_name]
    result = bash.run_command(cmd)
    user_id = int(result["stdout"])
    return user_id


def get_group_id_by_group_name(group_name: str) -> int:
    """Given a group name, return its group id"""
    # TODO: This code is incorrectly coded to get primary group id of a user,
    #  should use getent, but this is not available on Mac. Research.
    print(f"Getting group id by group name: {group_name}")
    cmd = ["id", "-g", group_name]
    result = bash.run_command(cmd)
    group_id = int(result["stdout"])
    return group_id


def get_latest_ansible_version_from_internet() -> str:
    """Go to the internet, determine the latest Ansible version available to install."""
    print(f"Getting latest Ansible version from internet")
    url = "https://pypi.org/pypi/ansible/json"
    request = urllib.request.urlopen(url)
    json_data = request.read().decode('utf-8')
    dict_data = json.loads(json_data)
    ansible_version = dict_data["info"]["version"]
    return ansible_version


###################################################################################################################


if __name__ == "__main__":
    main()
