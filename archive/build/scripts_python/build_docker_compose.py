import sys
from typing import Dict

from utils import screen_output_utils as screen
from data_files.JsonFile import JsonFile
from config.run_defaults import RUN_DEFAULTS


###################################################################################################################


def main():
    screen.heading_1("Build Config Values")

    # get config values data
    config_values_file = sys.argv[1]
    config_values = JsonFile(file_name=config_values_file)
    config_values.load()

    # get run config data
    run_config_file = sys.argv[2]
    run_config_data = {}
    if run_config_file == "":
        print("No run configuration file provided")
    else:
        run_config = JsonFile(file_name=run_config_file)
        run_config.load()
        run_config_data = run_config.data
    docker_compose_data = build_docker_compose_data(config_values.data, run_config_data)

    # save docker compose data
    docker_compose_file = sys.argv[3]
    docker_compose = JsonFile(file_name=docker_compose_file, read_only=False)
    docker_compose.data = docker_compose_data
    docker_compose.save()


###################################################################################################################


def build_docker_compose_data(config_values: Dict, run_config: Dict) -> Dict:
    """Give the build-generated config values, and the user-specified run config,
    generate the docker-compose file data."""
    print("Building docker compose data")

    # Start with default data
    data = RUN_DEFAULTS

    # Image name
    org_name = config_values['docker']['org_name']
    is_latest = config_values['ansible']['latest_version']
    version = "latest" if is_latest else config_values['ansible']['version']
    image_name = f"{org_name}/ansible:{version}"
    data["services"]["ansible"]["image"] = image_name

    # Copy this data as-is
    if "ansible_service" in run_config:
        ansible_service_data = run_config["ansible_service"]
        data["services"]["ansible"].update(ansible_service_data)

    return data


###################################################################################################################

if __name__ == "__main__":
    main()
