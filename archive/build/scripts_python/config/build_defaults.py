BUILD_DEFAULTS = {
    "docker": {
        "python_version": "3.10",
        "debian_codename": "bullseye",
        "org_name": "joeysbytes",
        "time_zone": "UTC"
    },
    "controller": {
        "user_id": 1000,
        "group_id": 1000
    },
    "ansible": {
        "version": "latest"
    },
    "files": {
        "config_values": "/tmp/ansible_tooling_build_config.json",
        "docker_compose": "/tmp/ansible_tooling_docker-compose.json"
    }
}
