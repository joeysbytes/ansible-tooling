RUN_DEFAULTS = {
    "services": {
        "ansible": {
            "container_name": "ansible",
            "hostname": "ansible-host",
            "restart": "unless-stopped",
            "network_mode": "host"
        }
    }
}
