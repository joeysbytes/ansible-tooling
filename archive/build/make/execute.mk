#----------------------------------------------------------------------------------------------------------------------
# Start / Stop / Execute steps
#----------------------------------------------------------------------------------------------------------------------

.PHONY: full-start
full-start: stop build-docker-compose start


.PHONY: start
start: _start show-docker-containers


.PHONY: _start
_start:
	@echo ""
	@echo "Starting Ansible container"
	@$(DOCKER_COMPOSE_CMD_PREFIX) up \
		--detach \
		--pull "never"


.PHONY: stop
stop: _stop show-docker-containers


.PHONY: _stop
_stop:
	@echo ""
	@echo "Stopping Ansible container"
	@$(DOCKER_COMPOSE_CMD_PREFIX) down \
		--timeout 1


.PHONY: bash
bash: _start _bash


.PHONY: full-bash
full-bash: full-start _bash


.PHONY: _bash
_bash:
	@echo ""
	@echo "Entering bash prompt within Ansible container"
	@$(DOCKER_COMPOSE_CMD_PREFIX) exec \
		ansible \
		/bin/bash
