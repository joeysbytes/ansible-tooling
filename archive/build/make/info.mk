#----------------------------------------------------------------------------------------------------------------------
# Show Information
#----------------------------------------------------------------------------------------------------------------------

.PHONY: help
help:
	@echo ""
	@cat "./build/make/help.txt"


.PHONY: show-docker-images
show-docker-images:
	@echo ""
	@docker images --filter label="application=ansible_tooling"


.PHONY: show-docker-containers
show-docker-containers:
	@echo ""
	@docker ps --all --filter label="application=ansible_tooling"


.PHONY: info
info: _start _info


.PHONY: _info
_info:
	@$(DOCKER_COMPOSE_CMD_PREFIX) exec \
		ansible \
		info.bash


.PHONY: show-make-values
show-make-values:
	@echo ""
	@echo "Makefile Variables and Values"
	@echo "============================="
	@echo "Input Config Files:"
	@echo "  BUILD_CONFIG_FILE:   $(BUILD_CONFIG_FILE)"
	@echo "  RUN_CONFIG_FILE:     $(RUN_CONFIG_FILE)"
	@echo ""
	@echo "Output Config Files:"
	@echo "  CONFIG_VALUES_FILE:  $(CONFIG_VALUES_FILE)"
	@echo "  DOCKER_COMPOSE_FILE: $(DOCKER_COMPOSE_FILE)"
	@echo ""
	@echo "Make Environment Variables:"
	@echo "  PYTHON3_EXECUTABLE:  $(PYTHON3_EXECUTABLE)"
