#----------------------------------------------------------------------------------------------------------------------
# Building of Docker components
#----------------------------------------------------------------------------------------------------------------------

.PHONY: build
build: show-make-values build-config-values build-docker-image prune-docker-images show-docker-images


.PHONY: full-build
full-build: stop build build-docker-compose


.PHONY: build-config-values
build-config-values:
	@echo ""
	@$(PYTHON3_EXECUTABLE) "./build/scripts_python/build_config_values.py" \
		"$(BUILD_CONFIG_FILE)" \
		"$(CONFIG_VALUES_FILE)"


.PHONY: build-docker-image
build-docker-image:
	@echo ""
	@$(PYTHON3_EXECUTABLE) "./build/scripts_python/build_docker_image.py" \
		"$(CONFIG_VALUES_FILE)" \
		"./build/Dockerfile"


.PHONY: build-docker-compose
build-docker-compose:
	@echo ""
	@$(PYTHON3_EXECUTABLE) "./build/scripts_python/build_docker_compose.py" \
		"$(CONFIG_VALUES_FILE)" \
		"$(RUN_CONFIG_FILE)" \
		"$(DOCKER_COMPOSE_FILE)"
