#----------------------------------------------------------------------------------------------------------------------
# Clean up Docker images
#----------------------------------------------------------------------------------------------------------------------

.PHONY: clean
clean: prune-docker-images show-docker-images


.PHONY: prune-docker-images
prune-docker-images:
	@echo ""
	@echo "Pruning Docker images"
	@docker image prune --force
