#######################################################################################################################
# Type 'make' by itself to get a help screen.
#######################################################################################################################

#----------------------------------------------------------------------------------------------------------------------
# Required make variables
#----------------------------------------------------------------------------------------------------------------------
# Scripts are built using bash
SHELL := /bin/bash

# Python executable
PYTHON_EXEC := python3

# Subdirectory where docker files are found
DOCKER_DIR := ./docker

# Name of the Docker Compose file to use
DOCKER_COMPOSE_FILE := docker-compose.yml

# Name of the environment file to use with the Docker compose file
DOCKER_COMPOSE_ENV_FILE := ./env/joeysbytes.env

# Gets the project name from the environment file
ANSIBLE_PROJECT_NAME := $(shell grep -e "^PROJ_NAME=" "$(DOCKER_DIR)/$(DOCKER_COMPOSE_ENV_FILE)"|cut -f2 -d=|cut -f2 -d\")

# This is used to start all docker compose commands
DOCKER_COMPOSE_CMD_PREFIX := cd "$(DOCKER_DIR)" && docker compose --file "$(DOCKER_COMPOSE_FILE)" --env-file "$(DOCKER_COMPOSE_ENV_FILE)"  --ansi "never" --project-name "$(ANSIBLE_PROJECT_NAME)"

# Gets the application name from the environment file
ANSIBLE_APPLICATION_NAME := $(shell grep -e "^APP_NAME=" "$(DOCKER_DIR)/$(DOCKER_COMPOSE_ENV_FILE)"|cut -f2 -d=|cut -f2 -d\")

# This is used to filter some docker commands
DOCKER_FILTER := --filter label=application=$(ANSIBLE_APPLICATION_NAME)


#----------------------------------------------------------------------------------------------------------------------
# Ansible Tooling Targets
#----------------------------------------------------------------------------------------------------------------------
.PHONY: help
help:
	@echo ""
	@cat ./make/help.txt
	@echo ""


#----------------------------------------------------------------------------------------------------------------------
# Building
#----------------------------------------------------------------------------------------------------------------------
.PHONY: build
build:  _build prune images

.PHONY: _build
_build:
	@echo "Building Docker image"
	@$(PYTHON_EXEC) ./make/get_ansible_ver.py "$(DOCKER_DIR)/$(DOCKER_COMPOSE_ENV_FILE)"
	@$(DOCKER_COMPOSE_CMD_PREFIX) build --progress plain --pull


#----------------------------------------------------------------------------------------------------------------------
# Execution
#----------------------------------------------------------------------------------------------------------------------
.PHONY: bash
bash:
	@echo "Entering bash prompt within container"
	@$(DOCKER_COMPOSE_CMD_PREFIX) run --rm ansible /bin/bash

.PHONY: down
down:
	@echo "Removing Docker container"
	@$(DOCKER_COMPOSE_CMD_PREFIX) down


#----------------------------------------------------------------------------------------------------------------------
# Cleaning
#----------------------------------------------------------------------------------------------------------------------
.PHONY: clean
clean: down prune containers

.PHONY: prune
prune:
	@echo "Pruning Docker images"
	@docker image prune --force $(DOCKER_FILTER)


#----------------------------------------------------------------------------------------------------------------------
# Information
#----------------------------------------------------------------------------------------------------------------------
.PHONY: images
images:
	@echo "Showing Docker images"
	@docker image ls $(DOCKER_FILTER)

.PHONY: containers
containers:
	@echo "Showing Docker containers"
	@$(DOCKER_COMPOSE_CMD_PREFIX) ps --all

.PHONY: info
info:
	@echo "Running info command from Docker container"
	@$(DOCKER_COMPOSE_CMD_PREFIX) run --rm ansible info
